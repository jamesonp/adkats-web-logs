<?php

$db = new mysqli('localhost', 'user', 'pass', 'statslogDB');
if($db->connect_errno > 0){
    die('Unable to connect to database [' . $db->connect_error . ']');
}

$sql = "SELECT record_time, command_name, target_name, source_name, record_message, server_id \n"
    . "FROM (adkats_records_main inner join adkats_commands on adkats_records_main.command_type = adkats_commands.command_id)\n"
    . "WHERE (command_type=6) AND `source_name` = 'PingEnforcer' \n"
    . "ORDER BY record_time DESC";
	 
if(!$result = $db->query($sql)){
    die('There was an error running the query [' . $db->error . ']');
}
echo '<html><head><title>Ping Kick Report</title><link rel="stylesheet" href="css/style.css"></head><body>';
	
	if ($result->num_rows > 0) {
    echo "<table><tr><th>Time</th><th title=\"Server Number\">S</th><th>Admin</th><th></th><th>Target</th><th>Message</th></tr>";
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "<tr><td class=\"shrink\">".$row["record_time"]."</td><td class=\"shrink\">".$row["server_id"]."</td><td class=\"shrink\">".$row["source_name"]."</td><td class=\"shrink\">".$row["command_name"]."</td><td class=\"shrink\">".$row["target_name"]."</td><td class=\"expand\">".$row["record_message"]."</td></tr>";
    }
    echo "</table>";
} else {
    echo "0 results";
	
	}
echo "</body></html>";
	
//Enable Error Logging
	//ini_set('display_errors',1);  
	//error_reporting(E_ALL);
	
mysqli_close($db);

?>